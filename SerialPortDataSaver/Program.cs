﻿using System;
using System.IO;
using System.IO.Ports;

class Program {
    static SerialPort serialPort;
    static string fileName;

    static void Main(string[] args) {
        Console.WriteLine("Podaj nazwę portu szeregowego:");
        string portName = Console.ReadLine();

        int portBaud;
        do {
            Console.WriteLine("Podaj prędkość transmisji sygnału:");
            try {
                portBaud = Convert.ToInt32(Console.ReadLine());
                break;
            } catch (Exception) {
                Console.WriteLine("\tMusisz podać liczbę całkowitą!");
            }            
        } while (true);

        fileName = DateTime.Now.ToString("yyyy.MM.dd HH-mm-ss ") + portName + ".csv";

        var file = new StreamWriter(fileName);
        file.WriteLine("Napięcie");
        file.Close();

        try {
            serialPort = new SerialPort(portName, portBaud);
            serialPort.DataReceived += SerialPortDataReceived;
            serialPort.Open();

            Console.WriteLine("Zbieram dane z portu {0} o częstotliwości {1} do pliku {2}", portName, portBaud, fileName);

        } catch (Exception e) {
            Console.WriteLine(e);
        }

        Console.ReadKey();

        if (serialPort.IsOpen) {
            serialPort.Close();
            serialPort.Dispose();
        }
    }

    private static void SerialPortDataReceived(object sender, SerialDataReceivedEventArgs e) {
        var file = new StreamWriter(fileName, true);
        string line = serialPort.ReadLine();
        file.Write(line);
        Console.WriteLine(line);
        file.Close();
    }
}
