#define readPin A0
#define diodaLED 13

void setup() {
  Serial.begin(9600);
  pinMode(readPin, INPUT);
  pinMode(diodaLED, OUTPUT);
}

void loop() {
  //double start = millis();
  double readPinValue = analogRead(readPin);
  
  double stalaMichala = 0.005838191431670;             // Po pół godzinnych obliczeniach, stała Michała opisuje nasz układ [27560 / 4720640]
  
  double voltage = stalaMichala * readPinValue;        // Do testów baterii -> napięcie baterii w naszym układzie
  Serial.println(voltage);
  
  int stanKrytycznyReadPin = 685;
  
  if (readPinValue < stanKrytycznyReadPin)
    digitalWrite(diodaLED, HIGH);
  else 
    digitalWrite(diodaLED, LOW);
  
  //double stop = millis();
  //double czasLoop = stop - start;
  //Serial.println(czasLoop);
  delay(10000);
}
